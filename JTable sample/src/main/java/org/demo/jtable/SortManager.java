/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.TableRowSorter;

import org.demo.jtable.model.CustomTableModel;

public class SortManager {
	private static final Comparator<?> TO_STRING_COMPARATOR = new ToStringComparator();

	private static final class TableRowSorterExtension extends TableRowSorter<CustomTableModel> {
		private TableRowSorterExtension(CustomTableModel model) {
			super(model);
		}

		@Override
		protected boolean useToString(int column) {
			return false;
		}

		@Override
		public Comparator<?> getComparator(int column) {
			return TO_STRING_COMPARATOR;
		}
	}

	private static final class AsendingSorter extends AbstractAction {

		private static final long serialVersionUID = 8948296269006863817L;
		private final JTable table;
		private SortOrder type;

		private AsendingSorter(String name, JTable table, SortOrder type) {
			super(name);
			this.table = table;
			this.type = type;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			int selectedColumn = table.getSelectedColumn();
			if (selectedColumn >= 0) {
				TableRowSorter<CustomTableModel> sorter = new TableRowSorterExtension(
						(CustomTableModel) table.getModel());
				List<RowSorter.SortKey> sortKeys = new ArrayList<>();
				sortKeys.add(new RowSorter.SortKey(selectedColumn, type));
				sorter.setSortKeys(Collections.unmodifiableList(sortKeys));
				table.setRowSorter(sorter);
			}
		}

	}

	public static void reBuild(JTable table) {
		JPopupMenu popup = new JPopupMenu();
		popup.add(new JMenuItem(
				new AsendingSorter(Messages.getString("SortManager.ascendingLabel"), table, SortOrder.ASCENDING))); //$NON-NLS-1$
		popup.add(new JMenuItem(
				new AsendingSorter(Messages.getString("SortManager.descendingLabel"), table, SortOrder.DESCENDING))); //$NON-NLS-1$
		table.setComponentPopupMenu(popup);
	}

	private SortManager() {
		// hide default constructor
	}
}
