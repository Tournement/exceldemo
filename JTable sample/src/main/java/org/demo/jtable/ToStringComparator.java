/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable;

import java.util.Comparator;

import org.demo.jtable.model.cell.CellModel;

public class ToStringComparator implements Comparator<Object> {

	@Override
	public int compare(Object o1, Object o2) {
		return extractValue(o1).compareToIgnoreCase(extractValue(o2));
	}

	private static String extractValue(Object o2) {
		if (o2 instanceof CellModel) {
			return String.valueOf(((CellModel)o2).getValue());
		}
		return String.valueOf(o2);
	}

}
