/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.demo.jtable.model.cell.CellModel;

public class DemoCellRenderer implements TableCellRenderer {

	private static final double FACTOR = 0.9;

	DefaultTableCellRenderer delegate = new DefaultTableCellRenderer();

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		DefaultTableCellRenderer tableCellRendererComponent = (DefaultTableCellRenderer) delegate.getTableCellRendererComponent(table, value, isSelected,
				hasFocus, row, column);
		CellModel cellModel = (CellModel) value;
		tableCellRendererComponent.setText(String.valueOf(cellModel.getValue()));
		Color background = isSelected ? getSelectionColor(table, cellModel) : getColor(table, cellModel);
		if (row % 2 == 0) {
			background = darker(background);
		}
		tableCellRendererComponent.setBackground(background);
		return tableCellRendererComponent;
	}

	private static Color getColor(JTable table, CellModel cellModel) {
		return cellModel.getBackgroundColor() == null ? table.getBackground() : cellModel.getBackgroundColor();
	}

	private static Color getSelectionColor(JTable table, CellModel cellModel) {
		return cellModel.getBackgroundColor() == null ? table.getSelectionBackground()
				: cellModel.getBackgroundColor().darker();
	}

	public static Color darker(Color color) {
		return new Color(Math.max((int) (color.getRed() * FACTOR), 0), Math.max((int) (color.getGreen() * FACTOR), 0),
				Math.max((int) (color.getBlue() * FACTOR), 0), color.getAlpha());
	}
}
