/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.action;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTable;

import org.demo.jtable.Messages;
import org.demo.jtable.model.CustomTableModel;
import org.demo.jtable.model.ModelLauncher;

public class SaveButtonActionListener extends AbstractButtonActionListener {

	private JFrame frame;

	public SaveButtonActionListener(JTable table, JFrame frame) {
		super(table);
		this.setFrame(frame);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle(Messages.getString("SaveButtonActionListener.SaveDialogTitle"));    //$NON-NLS-1$
		 
		int userSelection = fileChooser.showSaveDialog(getFrame());
		 
		if (userSelection == JFileChooser.APPROVE_OPTION) {
		    File fileToSave = fileChooser.getSelectedFile();
		    ModelLauncher.saveModel(fileToSave.getAbsolutePath(), ((CustomTableModel)getTable().getModel()).getModel());
		}
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

}
