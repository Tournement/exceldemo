/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.action;

import java.awt.event.ActionEvent;

import javax.swing.JTable;

import org.demo.jtable.model.CustomTableModel;
import org.demo.jtable.model.Model;
import org.demo.jtable.model.RowModel;
import org.demo.jtable.model.cell.CellFactory;

public class AddLineButtonActionListener extends AbstractButtonActionListener {

	public AddLineButtonActionListener(JTable table) {
		super(table);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Model model = ((CustomTableModel) getTable().getModel()).getModel();
		RowModel row = model.createRow();
		if(model.getColumnCount()==0) {
			CellFactory.createBlanckCell(row);
		}
	}
}
