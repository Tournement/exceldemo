/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.action;

import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JTable;

import org.demo.jtable.model.cell.CellModel;

public abstract class AbstractButtonActionListener implements ActionListener {

	private JTable table;

	public AbstractButtonActionListener(JTable table) {
		this.setTable(table);
	}

	public JTable getTable() {
		return table;
	}

	private void setTable(JTable table) {
		this.table = table;
	}

	public List<CellModel> getSelectedCells() {
		int[] selectedRow = table.getSelectedRows();
		int[] selectedColumns = table.getSelectedColumns();
		LinkedList<CellModel> output = new LinkedList<>();
		for (int i = 0; i < selectedRow.length; i++) {
			for (int j = 0; j < selectedColumns.length; j++) {
				if (table.isCellSelected(selectedRow[i], selectedColumns[j])) {
					output.add((CellModel) table.getValueAt(selectedRow[i], selectedColumns[j]));
				}
			}
		}
		return output;
	}

	protected int[] getSelectedColumns() {
		int[] viewSelectedColumn = getTable().getSelectedColumns();
		int[] modelSelectedColumn = new int[viewSelectedColumn.length];
		for (int i = 0; i < viewSelectedColumn.length; i++) {
			modelSelectedColumn[i] = getTable().convertColumnIndexToModel(viewSelectedColumn[i]);
		}
		return modelSelectedColumn;
	}

	protected int[] getSelectedRows() {
		int[] viewSelectedRow = getTable().getSelectedRows();
		int[] modelSelectedRow = new int[viewSelectedRow.length];
		for (int i = 0; i < viewSelectedRow.length; i++) {
			modelSelectedRow[i] = getTable().convertRowIndexToModel(viewSelectedRow[i]);
		}
		return modelSelectedRow;

	}
}
