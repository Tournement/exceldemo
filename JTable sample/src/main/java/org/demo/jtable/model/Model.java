/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.table.TableModel;

public class Model  implements Iterable<RowModel>{

	private List<RowModel> rowList;
	CustomTableModel tableModels;
	private Integer columnCount;

	public Model() {
		rowList = new ArrayList<>();
	}

	public TableModel getJtableBinder() {
		if (tableModels == null) {
			tableModels = new CustomTableModel(this);
		}
		return tableModels;
	}

	public RowModel createRow() {
		return createRow(getRowCount());
	}

	private RowModel createRow(int rowCount) {
		RowModel createRow = RowFactory.createRow(this);
		rowList.add(createRow);
		fireRowAdd(rowCount);
		return createRow;
	}

	void fireRowAdd(int rowCount) {
		tableModels.fireTableRowsInserted(rowCount, rowCount);
	}

	public int getRowCount() {
		return rowList.size();
	}

	public int getColumnCount() {
		return columnCount == null ? initColumnCount() : columnCount;
	}

	public int initColumnCount() {
		int max = 0;
		for (RowModel row : rowList) {
			max = Math.max(row.getSize(), max);
		}
		if (columnCount == null) {
			columnCount = max;
		} else {
			columnCount = Math.max(max, columnCount);
		}
		return columnCount;
	}

	public RowModel getRow(int rowIndex) {
		return rowList.get(rowIndex);
	}

	public void fireDataUpdated(int index, RowModel rowModel) {
		tableModels.fireTableCellUpdated(rowList.indexOf(rowModel), index);
	}

	public void fireRowCountChange() {
		tableModels.fireTableStructureChanged();
	}

	public void removeRow(int[] selectedRow) {
		List<RowModel> rowtoRemove = new LinkedList<>();
		for (int i = 0; i < selectedRow.length; i++) {
			rowtoRemove.add(rowList.get(selectedRow[i]));
		}
		for (RowModel row : rowtoRemove) {
			rowList.remove(row);
		}
		fireRowCountChange();
	}

	public void fireCellAdd(int index) {
		if (columnCount <= index) {
			fireNewColumnAdded(index);
		} else {
			tableModels.fireTableDataChanged();
		}
	}

	private void fireNewColumnAdded(int index) {
		columnCount = index + 1;
		tableModels.fireTableStructureChanged();
	}

	public void addColumn() {
		fireNewColumnAdded(getColumnCount());
	}

	public void removeColumn(int[] selectedColumn) {
		rowList.forEach(e -> e.removeColumns(selectedColumn));
		columnCount -= selectedColumn.length;
		tableModels.fireTableStructureChanged();
	}

	@Override
	public Iterator<RowModel> iterator() {
		return rowList.iterator();
	}
}
