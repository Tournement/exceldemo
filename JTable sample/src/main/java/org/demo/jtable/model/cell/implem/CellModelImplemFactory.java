/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.model.cell.implem;

import org.demo.jtable.model.cell.CellModel;
import org.demo.jtable.model.cell.ICellModel;

public class CellModelImplemFactory {
	private CellModelImplemFactory() {
		// hide default constructor
	}

	public static ICellModel createBlanckCell(CellModel cellModel) {
		return new BlankCellModel(cellModel);
	}

	public static ICellModel createStringCell(CellModel cellModel, String stringvalue) {
		return new StringCellModel(cellModel, stringvalue);
	}

	public static ICellModel createNumericCell(CellModel cellModel, double doubleValue) {
		return new NumericCellModel(cellModel, doubleValue);
	}

	public static ICellModel createFormulaCell(CellModel cellModel, String formulaValue) {
		return new FormulaCellModel(cellModel, formulaValue);
	}

	public static ICellModel createErrorCell(CellModel cellModel, byte errorCodeValue) {
		return new ErrorCellModel(cellModel, errorCodeValue);
	}

	public static ICellModel createBooleanCell(CellModel cellModel, boolean booleanValue) {
		return new BooleanCellModel(cellModel, booleanValue);
	}
}
