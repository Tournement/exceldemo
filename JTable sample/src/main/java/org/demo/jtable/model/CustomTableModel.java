/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.model;

import javax.swing.table.AbstractTableModel;

import org.demo.jtable.model.cell.CellModel;

public class CustomTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 4037916883330067258L;
	private transient Model model;

	public CustomTableModel(Model model) {
		this.setModel(model);
	}

	@Override
	public int getRowCount() {
		return model.getRowCount();
	}

	@Override
	public int getColumnCount() {
		return model.getColumnCount();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return model.getRow(rowIndex).getCell(columnIndex);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		model.getRow(rowIndex).getCell(columnIndex).setValue(String.valueOf(aValue));
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return CellModel.class;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

}
