/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.model.cell;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.demo.jtable.model.RowModel;

public class POICellModelfactory {
	private POICellModelfactory() {
		// hide defaul construcor
	}

	public static CellModel createCell(RowModel row, Cell cell) {
		switch (cell.getCellTypeEnum()) {
		case BOOLEAN:
			return CellFactory.createBooleanCell(row, cell.getBooleanCellValue());
		case ERROR:
			return CellFactory.createErrorCell(row, cell.getErrorCellValue());
		case FORMULA:
			return CellFactory.createFormulaCell(row, cell.getCellFormula());
		case NUMERIC:
			return CellFactory.createNumericCell(row, cell.getNumericCellValue());
		case STRING:
			return CellFactory.createStringCell(row, cell.getStringCellValue());
		default:
			return CellFactory.createBlanckCell(row);
		}
	}

	public static void createPIOCell(Row row, int i, CellModel celltowrite) {
		Cell cell = row.createCell(i);
		switch (celltowrite.getType()) {
		case BLANK:
			cell.setCellType(CellType.BLANK);
			break;
		case BOOLEAN:
			cell.setCellType(CellType.BOOLEAN);
			cell.setCellValue((boolean) celltowrite.getValue());
			break;
		case ERROR:
			cell.setCellType(CellType.ERROR);
			cell.setCellErrorValue((byte) celltowrite.getValue());
			break;
		case FORMULA:
			cell.setCellType(CellType.FORMULA);
			cell.setCellFormula((String) celltowrite.getValue());
			break;
		case NUMERIC:
			cell.setCellType(CellType.NUMERIC);
			cell.setCellValue((double) celltowrite.getValue());
			break;
		case STRING:
			cell.setCellType(CellType.STRING);
			cell.setCellValue((String) celltowrite.getValue());
			break;
		}
	}

}
