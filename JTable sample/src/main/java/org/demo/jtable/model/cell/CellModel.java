/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.model.cell;

import java.awt.Color;

import org.demo.jtable.model.RowModel;
import org.demo.jtable.model.cell.implem.CellModelImplemFactory;

public class CellModel implements ICellModel {
	private RowModel row;
	private ICellModel implementation;
	private Color backgroundColor;

	public CellModel(RowModel row) {
		this.setRow(row);
	}

	public RowModel getRowModel() {
		return row;
	}

	public void setRow(RowModel row) {
		this.row = row;
	}

	public ICellModel getImplementation() {
		return implementation;
	}

	public void setImplementation(ICellModel implementation, boolean mustFire) {
		this.implementation = implementation;
		getRowModel().add(this, mustFire);
	}

	@Override
	public Object getValue() {
		return implementation.getValue();
	}

	@Override
	public CellType getType() {
		return implementation.getType();
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public void setValue(String aValue) {
		if (aValue == null || aValue.trim().isEmpty()) {
			implementation = CellModelImplemFactory.createBlanckCell(this);
		} else {
			try {
				implementation = CellModelImplemFactory.createNumericCell(this, Double.parseDouble(aValue.trim()));
			} catch (NumberFormatException de) {
				if (aValue.trim().equalsIgnoreCase("true")) { //$NON-NLS-1$
					implementation = CellModelImplemFactory.createBooleanCell(this, true);
				} else if (aValue.equalsIgnoreCase("false")) { //$NON-NLS-1$
					implementation = CellModelImplemFactory.createBooleanCell(this, false);
				} else {
					try {
						implementation = CellModelImplemFactory.createNumericCell(this, Byte.parseByte(aValue.trim()));
					} catch (NumberFormatException be) {
						defaultSetStringValue(aValue);
					}
				}
			}
		}
	}

	private void defaultSetStringValue(String aValue) {
		if(implementation.getType()== CellType.FORMULA) {
			implementation = CellModelImplemFactory.createFormulaCell(this, aValue.trim());
		}else {
			implementation = CellModelImplemFactory.createStringCell(this, aValue.trim());
		}
	}
}
