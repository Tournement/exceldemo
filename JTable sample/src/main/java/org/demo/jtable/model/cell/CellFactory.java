/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.model.cell;

import org.demo.jtable.model.RowModel;
import org.demo.jtable.model.cell.implem.CellModelImplemFactory;

public class CellFactory {
	private CellFactory() {
		// hide default constructor
	}

	public static CellModel unfireCreateBlanckCell(RowModel row) {
		CellModel cellModel = new CellModel(row);
		cellModel.setImplementation(CellModelImplemFactory.createBlanckCell(cellModel), false);
		return cellModel;
	}

	public static CellModel createBlanckCell(RowModel row) {
		CellModel cellModel = new CellModel(row);
		cellModel.setImplementation(CellModelImplemFactory.createBlanckCell(cellModel), true);
		return cellModel;
	}

	public static CellModel createStringCell(RowModel row, String stringvalue) {
		CellModel cellModel = new CellModel(row);
		cellModel.setImplementation(CellModelImplemFactory.createStringCell(cellModel, stringvalue), true);
		return cellModel;
	}

	public static CellModel createNumericCell(RowModel row, double doubleValue) {
		CellModel cellModel = new CellModel(row);
		cellModel.setImplementation(CellModelImplemFactory.createNumericCell(cellModel, doubleValue), true);
		return cellModel;
	}

	public static CellModel createFormulaCell(RowModel row, String formulaValue) {
		CellModel cellModel = new CellModel(row);
		cellModel.setImplementation(CellModelImplemFactory.createFormulaCell(cellModel, formulaValue), true);
		return cellModel;
	}

	public static CellModel createErrorCell(RowModel row, byte errorCodeValue) {
		CellModel cellModel = new CellModel(row);
		cellModel.setImplementation(CellModelImplemFactory.createErrorCell(cellModel, errorCodeValue), true);
		return cellModel;
	}

	public static CellModel createBooleanCell(RowModel row, boolean booleanValue) {
		CellModel cellModel = new CellModel(row);
		cellModel.setImplementation(CellModelImplemFactory.createBooleanCell(cellModel, booleanValue), true);
		return cellModel;
	}

}
