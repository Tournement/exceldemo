/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.model.cell.implem;

import org.demo.jtable.model.cell.CellModel;
import org.demo.jtable.model.cell.CellType;

class ErrorCellModel extends AbstractCellModel {

	private byte errorCode;

	public ErrorCellModel(CellModel rowModel, byte errorCode) {
		super(rowModel);
		this.errorCode = errorCode;
	}

	@Override
	public Object getValue() {
		return errorCode;
	}

	@Override
	public CellType getType() {
		return CellType.ERROR;
	}

}
