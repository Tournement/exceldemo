/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.demo.jtable.model.cell.CellFactory;
import org.demo.jtable.model.cell.CellModel;

public class RowModel implements Iterable<CellModel>{

	private List<CellModel> cells;
	private Model model;

	public RowModel(Model model) {
		cells = new ArrayList<>();
		this.setModel(model);
	}

	public int getSize() {
		return cells.size();
	}

	public CellModel getCell(int columnIndex) {
		for (int i = cells.size(); i < columnIndex + 1; i++) {
			CellFactory.unfireCreateBlanckCell(this);
		}
		return cells.get(columnIndex);
	}

	public Model getModel() {
		return model;
	}

	private void setModel(Model model) {
		this.model = model;
	}

	public void add(CellModel cellModel) {
		add(cellModel, true);
	}

	public void add(CellModel cellModel, boolean mustFire) {
		this.cells.add(cellModel);
		if (mustFire) {
			fireCellAdd(cells.size() - 1);
		}
	}

	public void add(int index, CellModel cellModel) {
		add(index, cellModel, true);
	}

	public void add(int index, CellModel cellModel, boolean mustFire) {
		this.cells.add(index, cellModel);
		if (mustFire) {
			fireCellAdd(index);
		}
	}

	private void fireCellAdd(int index) {
		model.fireCellAdd(index);
	}

	public void removeColumns(int[] selectedColumn) {
		List<CellModel> cellToRemove = new LinkedList<>();
		for(int i = 0;i<selectedColumn.length;i++) {
			cellToRemove.add(cells.get(selectedColumn[i]));
		}
		for(CellModel elem : cellToRemove) {
			cells.remove(elem);
		}
	}

	@Override
	public Iterator<CellModel> iterator() {
		return cells.iterator();
	}

}
