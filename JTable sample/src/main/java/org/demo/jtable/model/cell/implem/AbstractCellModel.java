/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.model.cell.implem;

import org.demo.jtable.model.RowModel;
import org.demo.jtable.model.cell.CellModel;
import org.demo.jtable.model.cell.ICellModel;

 abstract class AbstractCellModel implements ICellModel {
	private CellModel container;

	public AbstractCellModel(CellModel cellModel) {
		this.setCellModel(cellModel);
	}

	private void setCellModel(CellModel cellModel) {
		container = cellModel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.demo.jtable.model.cell.ICellModel#getRowModel()
	 */
	@Override
	public RowModel getRowModel() {
		return container.getRowModel();
	}

}
