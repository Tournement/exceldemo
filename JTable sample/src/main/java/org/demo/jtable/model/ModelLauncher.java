/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.demo.jtable.model.cell.CellModel;
import org.demo.jtable.model.cell.POICellModelfactory;

public class ModelLauncher {
	private ModelLauncher() {
		// do nothing
	}

	public static Model readModel(String filepath) {
		String reolvePath = null;
		if (filepath == null) {
			reolvePath = System.getProperty("user.dir") + "/jtableDemo/default.xls"; //$NON-NLS-1$//$NON-NLS-2$
		} else {
			reolvePath = filepath;
		}
		Model model = new Model();
		try (FileInputStream f = new FileInputStream(new File(reolvePath)); Workbook workbook = new XSSFWorkbook(f)) {
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				RowModel row = model.createRow();
				Iterator<Cell> cellIterator = currentRow.iterator();
				while (cellIterator.hasNext()) {
					POICellModelfactory.createCell(row, cellIterator.next());
				}
			}
		} catch (IOException e) {
			//do nothing
		}
		return model;
	}

	public static void saveModel(String filepath, Model model) {
		String reolvePath = null;
		if (filepath == null) {
			reolvePath = System.getProperty("user.dir") + "/jtableDemo/default.xls"; //$NON-NLS-1$//$NON-NLS-2$
		} else {
			reolvePath = filepath;
		}
		File f = new File(reolvePath);
		// Create a Workbook
		try (Workbook workbook = new XSSFWorkbook()) { // new HSSFWorkbook() for generating `.xls` file

			// Create a Sheet
			Sheet sheet = workbook.createSheet("Sheet 1"); //$NON-NLS-1$

			// Create Other rows and cells with employees data
			int rowNum = 0;
			for (RowModel rowtowrite : model) {
				Row row = sheet.createRow(rowNum++);
				int colNum = 0;
				for (CellModel celltowrite : rowtowrite) {
					POICellModelfactory.createPIOCell(row, colNum++, celltowrite);
				}
			}

			// Resize all columns to fit the content size
			for (int i = 0; i < model.getColumnCount(); i++) {
				sheet.autoSizeColumn(i);
			}

			// Write the output to a file
			write(f, workbook);
		} catch (Exception e) {
			// do nothing
		}

	}

	private static void write(File f, Workbook workbook) {
		try (FileOutputStream fileOut = new FileOutputStream(f)) {
			workbook.write(fileOut);
		} catch (Exception e) {
			// do nothing
		}
	}
}
