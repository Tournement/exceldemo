/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.demo.jtable.action.AddColumnButtonActionListener;
import org.demo.jtable.action.AddLineButtonActionListener;
import org.demo.jtable.action.HighLightButtonActionListener;
import org.demo.jtable.action.RemoveColumnButtonActionListener;
import org.demo.jtable.action.RemoveLineButtonActionListener;
import org.demo.jtable.action.SaveButtonActionListener;
import org.demo.jtable.action.UnHighLightButtonActionListener;
import org.demo.jtable.model.Model;
import org.demo.jtable.model.ModelLauncher;
import org.demo.jtable.model.cell.CellModel;

public class GuiLauncher {

	public static void main(String[] args) {
		Model model = ModelLauncher.readModel(args.length > 0 ? args[0] : null);
		JFrame mainframe = createFrame(model.getJtableBinder(), createJTableColumnModel(), createJTableSelecionModel());
		mainframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainframe.pack();
		mainframe.setVisible(true);
	}

	private static ListSelectionModel createJTableSelecionModel() {
		return new DefaultListSelectionModel();
	}

	private static TableColumnModel createJTableColumnModel() {
		return new DefaultTableColumnModel();
	}

	public static JFrame createFrame(TableModel model, TableColumnModel column, ListSelectionModel selection) {
		JFrame frame = new JFrame(Messages.getString("GuiLauncher.FrameTitle")); //$NON-NLS-1$
		frame.setLayout(new BorderLayout());
		JTable table = createJTable(model, column, selection);

		frame.add(createTableContainer(table), BorderLayout.CENTER);
		frame.add(createButtonPanel(table, frame), BorderLayout.EAST);
		return frame;
	}

	private static Component createTableContainer(JTable table) {
		return new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	}

	private static JTable createJTable(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
		JTable jTable = new JTable(dm, cm, sm);
		jTable.setAutoCreateColumnsFromModel(true);
		jTable.setCellSelectionEnabled(true);
		jTable.setDefaultRenderer(CellModel.class, new DemoCellRenderer());
		jTable.setDefaultEditor(CellModel.class, new DemoCellEditor());
		SortManager.reBuild(jTable);
		return jTable;
	}

	private static Component createButtonPanel(JTable table, JFrame frame) {
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), Messages.getString("GuiLauncher.ActionPanelTitle"))); //$NON-NLS-1$
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = createAndInitGridBagConstraints();
		LinkedList<Component> buttonToAdd = new LinkedList<>();
		buttonToAdd.add(createHighLightButton(table));
		buttonToAdd.add(createUnHighLightButton(table));
		buttonToAdd.add(createAddLineButton(table));
		buttonToAdd.add(createRemoveLineButton(table));
		buttonToAdd.add(createAddColumnLineButton(table));
		buttonToAdd.add(createRemoveColumnLineButton(table));
		for (Component button : buttonToAdd) {
			panel.add(button, c);
			c.gridy++;
		}
		c.weighty=1;
		panel.add(new JLabel(),c);
		c.weighty = 0;
		c.gridy++;
		panel.add(createSaveButton(table, frame),c);
		return panel;
	}

	private static GridBagConstraints createAndInitGridBagConstraints() {
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.weighty = 0;
		c.ipady=5;
		c.insets = new Insets(2, 4, 4, 2);
		return c;
	}

	private static Component createSaveButton(JTable table, JFrame frame) {
		JButton button = new JButton(Messages.getString("GuiLauncher.save")); //$NON-NLS-1$
		button.addActionListener(new SaveButtonActionListener(table, frame));
		return button;
	}

	private static Component createUnHighLightButton(JTable table) {
		JButton button = new JButton(Messages.getString("GuiLauncher.Unhighlight")); //$NON-NLS-1$
		button.addActionListener(new UnHighLightButtonActionListener(table));
		return button;
	}

	private static Component createRemoveColumnLineButton(JTable table) {
		JButton button = new JButton(Messages.getString("GuiLauncher.RemoveColumn")); //$NON-NLS-1$
		button.addActionListener(new RemoveColumnButtonActionListener(table));
		return button;
	}

	private static Component createAddColumnLineButton(JTable table) {
		JButton button = new JButton(Messages.getString("GuiLauncher.AddColumn")); //$NON-NLS-1$
		button.addActionListener(new AddColumnButtonActionListener(table));
		return button;
	}

	private static Component createRemoveLineButton(JTable table) {
		JButton button = new JButton(Messages.getString("GuiLauncher.RemoveLineButton")); //$NON-NLS-1$
		button.addActionListener(new RemoveLineButtonActionListener(table));
		return button;
	}

	private static Component createAddLineButton(JTable table) {
		JButton button = new JButton(Messages.getString("GuiLauncher.AddLineButton")); //$NON-NLS-1$
		button.addActionListener(new AddLineButtonActionListener(table));
		return button;
	}

	private static Component createHighLightButton(JTable table) {
		JButton button = new JButton(Messages.getString("GuiLauncher.HightLightButton")); //$NON-NLS-1$
		button.addActionListener(new HighLightButtonActionListener(table));
		return button;
	}
}
