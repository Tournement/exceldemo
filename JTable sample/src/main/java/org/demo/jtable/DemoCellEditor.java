/*******************************************************************************
 * Copyright (C) 2018 Arnault Le Pr�vost-Corvellec
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.demo.jtable;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;

import org.demo.jtable.model.cell.CellModel;

public class DemoCellEditor extends DefaultCellEditor {

	private static final long serialVersionUID = 138504653668773641L;

	public DemoCellEditor() {
		this(new JTextField());
	}

	public DemoCellEditor(JTextField textField) {
		super(textField);
		((JTextField) editorComponent).removeActionListener(delegate);

		delegate = new EditorDelegate() {

			private static final long serialVersionUID = 24123441750342293L;

			@Override
			public void setValue(Object value) {
				textField.setText(String.valueOf(((CellModel) value).getValue()));
			}

			@Override
			public Object getCellEditorValue() {
				return textField.getText();
			}
		};
		textField.addActionListener(delegate);
	}

	@Override
	public Component getComponent() {
		return editorComponent;
	}

}
